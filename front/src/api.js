const root = "/api";

export default {
  async login(email, password) {
    const result = await fetch(`${root}/log/user`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }).then(response => response.json());
    return await result;
  }
};
