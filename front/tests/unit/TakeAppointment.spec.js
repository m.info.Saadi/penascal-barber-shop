import { shallowMount } from "@vue/test-utils";
import TakeAppointment from "@/components/TakeAppointment.vue";

test("Este el Button de anadir ", async () => {
  const Contact = {
    email: "saadi@gmail.com",
    password: "123"
  };
  const wrapper = shallowMount(TakeAppointment, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.email = Contact.email;
  wrapper.vm.password = Contact.user;

  const button = wrapper.find(".btnIniciarSesion");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "btnIniciarSesion",
      args: ["saadi@gmail.com", "123"]
    }
  ]);
});
