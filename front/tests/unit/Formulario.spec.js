import { shallowMount } from "@vue/test-utils";
import Formulario from "@/components/Formulario.vue";

test("Este el Button de anadir ", async () => {
  const events = {
    title: Cortar-pello,
            Peluquero: peluquero1,
            start: "2020-06-20",
            end: "2020-06-21",
            classe:" 10" 
  };
  const wrapper = shallowMount(Formulario, {});
  expect(wrapper.emittedByOrder()).toEqual([]);
  wrapper.vm.title = events.title;
  wrapper.vm.peluquero = events.peluquero;
  wrapper.vm.start = events.start;
  wrapper.vm.end = events.end;
  wrapper.vm.classe = events.classe;

  const button = wrapper.find(".btnadd");
  button.trigger("click");
  await wrapper.vm.$nextTick();

  console.log("emmitedByOrder", wrapper.emittedByOrder());
  expect(wrapper.emittedByOrder()).toEqual([
    {
      name: "conferm-appointment",
      args: ["Cortar-pello", "peluquero1", "2020-06-20", "2020-06-21", "10"]
    }
  ]);
});

