import { shallowMount } from "@vue/test-utils";
import Header from "@/components/Header.vue";

test("Comprobar el componente Header", () => {
  //crear componente con las propiedades

  const wrapper = shallowMount(Header);

  expect(wrapper.emitted().search).toBe(undefined);
  //buscamos la clase de la plantilla html o el componente  con (findAll)

  const items = wrapper.findAll(".header").wrappers;

  wrapper.findAll(".header").wrappers;

  //para acceder dentro del wrapper usamos .vm
  expect(items.length).toBe(1);
});


