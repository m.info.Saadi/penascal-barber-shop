<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class eventsApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  events (
                id	TEXT NOT NULL,
                title	TEXT,
                peluquero	TEXT,
                start	TEXT,
                end	TEXT,
                classe TEXT,
            );
            INSERT INTO events VALUES ('1','cortar pello', 'marck','2020-07-01 14:30','2020-07-01 15:30','20');
            INSERT INTO events VALUES ('2','cortar pello', 'jack','2020-07-01 15:30','2020-07-01 16:30','20');
        ");
        // $this->withoutExceptionHandling();
    }

    public function testGetevents()
    {
        $this->json('GET', 'api/events')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',
                ],
                [
                    'id' => '2',
                    'title' => 'cortar pello',
                    'peluquero' => 'jack',
                    'start' => '2020-07-01 15:30',
                    'end' => '2020-07-01 16:30',
                    'classe' => '20',
                ],
            ]);
    }

    public function testGetevent()
    {
        $this->json('GET', 'api/events/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',
                ]
            );
    }

    public function testPostContact()
    {
        $this->json('POST', 'api/events', [

                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',

        ])->assertStatus(200);

        $this->json('GET', 'api/events/99')
            ->assertStatus(200)
            ->assertJson([

                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',
            ]);
    }

    public function testPut()
    {
        $data = [
                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',
        ];

        $expected = [

                    'id' => '1',
                    'title' => 'cortar pello',
                    'peluquero' => 'marck',
                    'start' => '2020-07-01 14:30',
                    'end' => '2020-07-01 15:30',
                    'classe' => '20',
        ];

        $this->json('PUT', 'api/events/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/events/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

    public function testPatch()
    {
        $this->json('PATCH', 'api/events/1', [
            'classe' => '30',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'title' => 'cortar pello',
                'peluquero' => 'marck',
                'start' => '2020-07-01 14:30',
                'end' => '2020-07-01 15:30',
                'classe' => '30',
            ]);

        $this->json('GET', 'api/events/1')
            ->assertStatus(200)
            ->assertJson([
                'title' => 'cortar pello y barba',
            ]);

        $this->json('PATCH', 'api/events/1', [
            'peluquero' => 'moha',
                'start' => '2020-08-01 14:30',
                'end' => '2020-08-01 15:30',
                'classe' => '30',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'title' => 'cortar pello y barba',
                'peluquero' => 'marck',
                'start' => '2020-08-01 14:30',
                'end' => '2020-08-01 15:30',
                'classe' => '30',
            ]);

        $this->json('GET', 'api/events/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'title' => 'cortar pello y barba',
                'peluquero' => 'marck',
                'start' => '2020-08-01 14:30',
                'end' => '2020-08-01 15:30',
                'classe' => '30',
            ]);
    }


     public function testDeleteContact()
    {
        $this->json('GET', 'api/events/1')->assertStatus(200);

        $this->json('DELETE', 'api/events/1')->assertStatus(200);

        $this->json('GET', 'api/events/1')->assertStatus(404);
    }

    public function testGetContactNotExist()
    {
        // $this->json('GET', 'api/events/22')->assertStatus(404);

        $this->json('DELETE', 'api/events/22')->assertStatus(404);

        $this->json(
            'PUT',
            'api/events/22',
            [
                'id' => '1',
                'title' => 'cortar pello y barba',
                'peluquero' => 'marck',
                'start' => '2020-08-01 14:30',
                'end' => '2020-08-01 15:30',
                'classe' => '30',
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/events/22', [
            'title' => 'cortar pello y barba',
        ])
            ->assertStatus(404);
    } 
}