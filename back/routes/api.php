<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/events', function (Request $request) {
    $results = DB::select('select * from events');
    return response()->json($results, 200);
});
/* 
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::get('/events/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (carNotExists($id)) {
        abort(404);
    }

    // Realizamos la consulta de la base de datos
    $results = DB::select('select * from events where id=:id', [
        'id' => $id,
    ]);

    // Así se devuelve un JSON.
    // El primer parámetro es el dato que se manda como JSON.
    // El segundo parámetro es el status code
    return response()->json($results[0], 200);
});

Route::post('/events', function () {
    // Así se recogen los datos que llegan de la petición
    $data = request()->all();

    DB::insert(
        "
        insert into events (id, peluquero,
        title,
        start,
        end,
        classe)
        values (:id, :peluquero, :title, :start, :end, :classe)
    ",
        $data
    );

    $results = DB::select('select * from events where id = :id', [
        'id' => $data['id'],
    ]);
    return response()->json($results[0], 200);
});

/* Route::get('/events/search/{inputDato}', function($inputDato) {
    //LIKE sql 
    $results = DB::select('select * from events where firstName LIKE "%' . $inputDato .cd '%"');
    return response()->json($results, 200);
}); */

Route::put('/events/{id}', function ($id) {
    if (carNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    DB::delete(
        "
        delete from events where id = :id",
        ['id' => $id]
    );

    DB::insert(
        "
        insert into events (id, peluquero,
        title,
        start,
        end,
        classe)
        values (:id, :peluquero, :title, :start, :end, :classe)
    ",
        $data
    );

    $results = DB::select('select * from events where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::patch('/events/{id}', function ($id) {
    if (carNotExists($id)) {
        abort(404);
    }
    $data = request()->all();

    $update_statements = array_map(function ($key) {
        return "$key = :$key";
    }, array_keys($data));

    $data['id'] = $id;

    DB::update(
        'update events SET ' . join(', ', $update_statements) . ' where id = :id',
        $data
    );

    $results = DB::select('select * from events where id = :id', ['id' => $id]);
    return response()->json($results[0], 200);
});

Route::delete('/events/{id}', function ($id) {
    // Devolvemos un error con status code 404 si no hay registros en la tabla
    if (carNotExists($id)) {
        abort(404);
    }

    DB::delete('delete from events where id = :id', ['id' => $id]);

    return response()->json('', 200);
});

// Laravel tiene un fallo y carga varias veces este archivo,
// provocando un error si se declara una función (cannot redeclare function).
// Para solventarlo, utilizamos este truquito

if (!function_exists('carNotExists')) {
    function carNotExists($id)
    {
        $results = DB::select('select * from events where id=:id', [
            'id' => $id,
        ]);

        return count($results) == 0;
    }
}

Route::post('/log/Admin/', function () {
    $data = request()->all();
    $mailExist = DB::select('select * from Userwhere upper(Email_Admin)=upper(:Email_Admin)', [
        'Email_Admin' =>$data['Email_Admin'],
     ]);
    $mailExist = sizeof($mailExist);
    if($mailExist == 0 ){ 
    $results= [ 
        'log_mensajes' => 'correo incorrecto',
    ];
    return response()->json($results, 200); 
    }else{  
        $passwordTrue = DB::select('select Pssword_Userfrom Userwhere upper(Email_Admin)=upper(:Email_Admin)', [
            'Email_Admin' =>$data['Email_Admin'],
            ]);
        $arr = []; 
        foreach($passwordTrue as $raw){
            $arr[]=(array)$raw;
        }
    if($data['Pssword_Admin']==$arr[0]['Pssword_Admin']){
    $results = DB::select('select idadmin, role from Userwhere upper(Email_Admin)=upper(:Email_Admin) ', [
        'Email_Admin' =>$data['Email_Admin'],
    ]);
    return response()->json($results[0], 200); 
    }else
    $results= 
    [ 
    'log_mensajes' => 'contraseña incorrecta',
    ];
       return response()->json($results, 200);
    }
});




Route::post('/log/user', function () {
    $data = request()->all();
    $mailExist = DB::select('select * from user where upper(email)=upper(:email)', [
        'email' =>$data['email'],
     ]);
    $mailExist = sizeof($mailExist);
    if($mailExist == 0 ){ 
    $results= [ 
        'log_mensajes' => 'correo incorrecto',
    ];
    return response()->json($results, 200); 
    }else{  
        $passwordTrue = DB::select('select password from user where upper(email)=upper(:email)', [
            'email' =>$data['email'],
            ]);
        $arr = []; 
        foreach($passwordTrue as $raw){
            $arr[]=(array)$raw;
        }
    if($data['password']==$arr[0]['password']){
    $results = DB::select('select idUser, role from user where upper(email)=upper(:email) ', [
        'email' =>$data['email'],
    ]);
    return response()->json($results[0], 200); 
    }else
    $results= 
    [ 
    'log_mensajes' => 'contraseña incorrecta',
    ];
       return response()->json($results, 200);
    }
});


